package redis

import (
	"fmt"
	"strconv"
	"strings"
)

const SEP = ":"

func IDToString(id int64) string {
	return strconv.FormatInt(id, 10)
}

func JoinKeyPath(keys ...interface{}) string {
	strs := make([]string, len(keys))

	for i, key := range keys {
		switch val := key.(type) {
		case int64:
			strs[i] = IDToString(val)
		case string:
			strs[i] = val
		case StatKind:
			strs[i] = string(val)
		default:
			panic(fmt.Sprintf("unexpected type: %T", val))
		}
	}

	return strings.Join(strs, SEP)
}

func SplitKeyPath(path string) []string {
	return strings.Split(path, SEP)
}

func ParseID(key string) int64 {
	id, err := strconv.ParseInt(key, 10, 64)
	if err != nil {
		panic(err)
	}

	return id
}
