package redis

import (
	"fmt"
	"log"
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/eantyshev/go.krutilka/internal/entity"
	"gitlab.com/eantyshev/go.krutilka/internal/repository"
)

type StatKind string

const (
	StatClicks StatKind = "clicks"
	StatShows  StatKind = "shows"
)

type Repo struct {
	pool *redis.Pool
}

var _ repository.RepoIface = &Repo{}

func NewRedisRepo(redisAddr string, idleSize int, idleTimeout time.Duration) *Repo {
	pool := &redis.Pool{
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", redisAddr)
		},
		MaxIdle:     idleSize,
		IdleTimeout: idleTimeout,
	}

	return &Repo{pool: pool}
}

func (r *Repo) AddSlotBanner(slotID, bannerID int64) error {
	keySlotPath := JoinKeyPath("slot", slotID, "banners")
	keyBannerPath := JoinKeyPath("banner", bannerID, "slots")
	conn := r.pool.Get()

	defer conn.Close()

	exists, err := redis.Bool(conn.Do("SISMEMBER", keySlotPath, bannerID))
	if err != nil {
		return err
	}

	if exists {
		return entity.ErrBannerExists
	}

	err = conn.Send("MULTI")
	if err != nil {
		return err
	}

	err = conn.Send("SADD", keySlotPath, bannerID)
	if err != nil {
		return err
	}

	err = conn.Send("SADD", keyBannerPath, slotID)
	if err != nil {
		return err
	}

	_, err = conn.Do("EXEC")

	return err
}

func (r *Repo) RemoveSlotBanner(slotID, bannerID int64) error {
	keySlotPath := JoinKeyPath("slot", slotID, "banners")
	keyBannerPath := JoinKeyPath("banner", bannerID, "slots")
	conn := r.pool.Get()

	defer conn.Close()

	exists, err := redis.Bool(conn.Do("SISMEMBER", keySlotPath, bannerID))
	if err != nil {
		return err
	} else if !exists {
		return entity.ErrBannerNotFound
	}

	err = conn.Send("MULTI")
	if err != nil {
		return err
	}

	err = conn.Send("SREM", keySlotPath, bannerID)
	if err != nil {
		return err
	}

	err = conn.Send("SREM", keyBannerPath, slotID)
	if err != nil {
		return err
	}

	_, err = conn.Do("EXEC")

	return err
}

func (r *Repo) SetBannerDescription(bannerID int64, description string) error {
	keyPath := JoinKeyPath("banner", bannerID, "description")
	conn := r.pool.Get()

	defer conn.Close()

	_, err := conn.Do("SET", keyPath, description)

	return err
}

func (r *Repo) DelBannerDescription(bannerID int64) error {
	keyPath := JoinKeyPath("banner", bannerID, "description")
	conn := r.pool.Get()

	defer conn.Close()

	_, err := conn.Do("DEL", keyPath)

	return err
}

func (r *Repo) ListSlotBanners(slotID int64) ([]int64, error) {
	keyPath := JoinKeyPath("slot", slotID, "banners")
	conn := r.pool.Get()

	defer conn.Close()

	values, err := redis.Values(conn.Do("SMEMBERS", keyPath))
	if err != nil {
		return nil, err
	}

	var bannerIDs []int64
	if err := redis.ScanSlice(values, &bannerIDs); err != nil {
		return nil, err
	}

	return bannerIDs, nil
}

func (r *Repo) CheckSlotHasBanner(slotID, bannerID int64) (bool, error) {
	keyPath := JoinKeyPath("slot", slotID, "banners")
	conn := r.pool.Get()

	defer conn.Close()

	return redis.Bool(conn.Do("SISMEMBER", keyPath, bannerID))
}

func (r *Repo) ListBannerSlots(bannerID int64) ([]int64, error) {
	keyPath := JoinKeyPath("banner", bannerID, "slots")
	conn := r.pool.Get()

	defer conn.Close()

	return redis.Int64s(conn.Do("SMEMBERS", keyPath))
}

func (r *Repo) IncrStats(groupID, bannerID int64, kind StatKind) error {
	groupsPath := JoinKeyPath("groups")
	groupBannersPath := JoinKeyPath("group", groupID, "banners")
	statPath := JoinKeyPath("stat", groupID)
	statKey := JoinKeyPath(bannerID, kind)
	conn := r.pool.Get()

	defer conn.Close()

	log.Println("SADD", groupsPath, groupID)
	log.Println("SADD", groupBannersPath, bannerID)
	log.Println("HINCRBY", statPath, statKey, 1)

	if err := conn.Send("MULTI"); err != nil {
		return err
	}

	if err := conn.Send("SADD", groupsPath, groupID); err != nil {
		return err
	}

	if err := conn.Send("SADD", groupBannersPath, bannerID); err != nil {
		return err
	}

	if err := conn.Send("HINCRBY", statPath, statKey, 1); err != nil {
		return err
	}

	_, err := conn.Do("EXEC")

	return err
}

func (r *Repo) IncrClicks(groupID, bannerID int64) error {
	return r.IncrStats(groupID, bannerID, StatClicks)
}

func (r *Repo) IncrShows(groupID, bannerID int64) error {
	return r.IncrStats(groupID, bannerID, StatShows)
}

func (r *Repo) ClearGroupBannerStats(groupID, bannerID int64) error {
	groupBannersPath := JoinKeyPath("group", groupID, "banners")
	statPath := JoinKeyPath("stat", groupID)
	conn := r.pool.Get()

	defer conn.Close()

	includesBanner, err := redis.Bool(conn.Do("SREM", groupBannersPath, bannerID))
	if err != nil {
		return err
	}

	if includesBanner {
		args := redis.Args{}.Add(statPath,
			JoinKeyPath(bannerID, StatClicks),
			JoinKeyPath(bannerID, StatShows),
		)
		log.Println("HDEL", args)

		_, err = conn.Do("HDEL", args...)
		if err != nil {
			return err
		}
	}

	return nil
}

// ClearBannerStats clears banner's stats from all groups
func (r *Repo) ClearBannerStats(bannerID int64) error {
	groupsPath := JoinKeyPath("groups")
	conn := r.pool.Get()

	defer conn.Close()

	groups, err := redis.Int64s(conn.Do("SMEMBERS", groupsPath))
	if err != nil {
		return err
	}

	for _, groupID := range groups {
		statPath := JoinKeyPath("stat", groupID)

		if err := r.ClearGroupBannerStats(groupID, bannerID); err != nil {
			return err
		}

		groupExists, err := redis.Bool(conn.Do("EXISTS", statPath))
		if err != nil {
			return err
		}

		if !groupExists {
			_, err := conn.Do("SREM", groupsPath, groupID)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (r *Repo) ListStats(groupID int64) ([]*entity.Stat, error) {
	statPath := JoinKeyPath("stat", groupID)
	conn := r.pool.Get()

	defer conn.Close()

	log.Println("HGETALL", statPath)

	statsRaw, err := redis.Int64Map(conn.Do("HGETALL", statPath))
	if err != nil {
		return nil, err
	}

	statMap := make(map[int64]*entity.Stat)

	for name, cnt := range statsRaw {
		bc := SplitKeyPath(name)
		bannerID := ParseID(bc[0])
		kind := bc[1]

		stat, ok := statMap[bannerID]
		if !ok {
			stat = &entity.Stat{BannerID: bannerID}
			statMap[bannerID] = stat
		}

		switch StatKind(kind) {
		case StatClicks:
			stat.Clicks = cnt
		case StatShows:
			stat.Shows = cnt
		default:
			return nil, fmt.Errorf("no such stat type %s", kind)
		}
	}

	results := make([]*entity.Stat, 0, len(statMap))
	for _, val := range statMap {
		results = append(results, val)
	}

	return results, nil
}
