package repository

import "gitlab.com/eantyshev/go.krutilka/internal/entity"

type RepoIface interface {
	ListStats(groupID int64) ([]*entity.Stat, error)
	AddSlotBanner(slotID, bannerID int64) error
	RemoveSlotBanner(slotID, bannerID int64) error

	SetBannerDescription(bannerID int64, description string) error
	DelBannerDescription(bannerID int64) error

	ListSlotBanners(slotID int64) ([]int64, error)
	CheckSlotHasBanner(slotID, bannerID int64) (bool, error)
	ListBannerSlots(bannerID int64) ([]int64, error)

	IncrClicks(groupID, bannerID int64) error
	IncrShows(groupID, bannerID int64) error
	ClearBannerStats(bannerID int64) error
}
