package queue

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.com/eantyshev/go.krutilka/internal/entity"

	"github.com/streadway/amqp"
)

func failOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

func connectAmqp(dsn string, connectTimeout, retryPeriod time.Duration) (*amqp.Connection, error) {
	deadline := time.Now().Add(connectTimeout)
	for deadline.After(time.Now()) {
		conn, err := amqp.Dial(dsn)
		if err == nil {
			return conn, nil
		}

		log.Println("rabbitmq:", err)
		time.Sleep(retryPeriod)
	}

	return nil, fmt.Errorf("timeout exceeded")
}

type RabbitQueue struct {
	*amqp.Channel
	exchangeName, queueName string
}

func NewRabbitQueue(
	dsn, exchangeName, queueName string,
	connectTimeout, retryPeriod time.Duration,
) *RabbitQueue {
	conn, err := connectAmqp(dsn, connectTimeout, retryPeriod)
	failOnErr(err)

	ch, err := conn.Channel()
	failOnErr(err)

	n := &RabbitQueue{
		Channel:      ch,
		exchangeName: exchangeName,
		queueName:    queueName,
	}
	n.Setup()

	return n
}

func (n *RabbitQueue) Setup() {
	// declare fanout exchange
	err := n.Channel.ExchangeDeclare(
		n.exchangeName, // name
		"fanout",       // type
		true,           // durable
		false,          // auto-deleted
		false,          // internal
		false,          // no-wait
		nil,            // arguments
	)
	failOnErr(err)

	// declare a durable queue
	q, err := n.Channel.QueueDeclare(
		n.queueName, // name
		false,       // durable
		false,       // delete when unused
		false,       // exclusive
		false,       // no-wait
		nil,         // arguments
	)
	failOnErr(err)

	err = n.Channel.QueueBind(
		q.Name,         // queue name
		"",             // routing key, ignored by fanout exchange
		n.exchangeName, // exchange
		false,
		nil)
	failOnErr(err)
}

func (n *RabbitQueue) Send(event *entity.BannerEvent) error {
	data, err := json.Marshal(event)
	failOnErr(err)

	return n.Channel.Publish(
		n.exchangeName,
		"",
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         data,
		})
}

func (n *RabbitQueue) ShowEvent(groupID, slotID, bannerID int64) error {
	event := &entity.BannerEvent{
		Timestamp: time.Now(),
		Type:      entity.ShowEvent,
		GroupID:   groupID,
		SlotID:    slotID,
		BannerID:  bannerID,
	}

	return n.Send(event)
}

func (n *RabbitQueue) ClickEvent(groupID, slotID, bannerID int64) error {
	event := &entity.BannerEvent{
		Timestamp: time.Now(),
		Type:      entity.ClickEvent,
		GroupID:   groupID,
		SlotID:    slotID,
		BannerID:  bannerID,
	}

	return n.Send(event)
}
