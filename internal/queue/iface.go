package queue

type Service interface {
	ClickEvent(groupID, slotID, bannerID int64) error
	ShowEvent(groupID, slotID, bannerID int64) error
}
