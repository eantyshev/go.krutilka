package usecase

type Iface interface {
	AddBanner(slotID, bannerID int64, description string) error
	RemoveBanner(slotID, bannerID int64) error
	ClickBanner(groupID, slotID, bannerID int64) error
	ChooseBanner(groupID, slotID int64) (int64, error)
}
