package usecase

import (
	"log"
	"sort"

	"gitlab.com/eantyshev/go.krutilka/internal/entity"
	"gitlab.com/eantyshev/go.krutilka/internal/queue"
	"gitlab.com/eantyshev/go.krutilka/internal/repository"
	"gitlab.com/eantyshev/go.krutilka/internal/ucb1"
)

type Usecase struct {
	Repo  repository.RepoIface
	Queue queue.Service
}

var _ Iface = &Usecase{}

func (u *Usecase) AddBanner(slotID, bannerID int64, description string) error {
	var err error
	if err = u.Repo.AddSlotBanner(slotID, bannerID); err != nil {
		return err
	}

	if description > "" {
		err = u.Repo.SetBannerDescription(bannerID, description)
	}

	return err
}

func (u *Usecase) RemoveBanner(slotID, bannerID int64) error {
	if err := u.Repo.RemoveSlotBanner(slotID, bannerID); err != nil {
		return err
	}

	slots, err := u.Repo.ListBannerSlots(bannerID)
	if err != nil {
		return err
	}

	if len(slots) == 0 {
		log.Println("no slots for banner, remove description")

		if err := u.Repo.DelBannerDescription(bannerID); err != nil {
			return err
		}
	}

	return u.Repo.ClearBannerStats(bannerID)
}

func (u *Usecase) ChooseBanner(groupID, slotID int64) (int64, error) {
	var bannerID int64

	bannerIDs, err := u.Repo.ListSlotBanners(slotID)

	if err != nil {
		return 0, err
	}

	if len(bannerIDs) == 0 {
		return 0, entity.ErrNoSlotBanners
	}

	stats, err := u.Repo.ListStats(groupID)
	if err != nil {
		return 0, err
	}

	log.Println("stats:")

	for _, s := range stats {
		log.Printf("%#v\n", s)
	}

	stats = FilterStats(bannerIDs, stats)
	bannerID = ucb1.Choose(stats)
	log.Println("chosen banner", bannerID)

	err = u.Repo.IncrShows(groupID, bannerID)
	if err != nil {
		return bannerID, err
	}

	// graceful degradation here: only show error message if failed here
	if err = u.Queue.ShowEvent(groupID, slotID, bannerID); err != nil {
		log.Println("rabbitmq ERROR:", err)
	}

	return bannerID, nil
}

func (u *Usecase) ClickBanner(groupID, slotID, bannerID int64) error {
	exists, err := u.Repo.CheckSlotHasBanner(slotID, bannerID)
	if err != nil {
		return err
	}

	if !exists {
		return entity.ErrBannerNotFound
	}

	err = u.Repo.IncrClicks(groupID, bannerID)
	if err != nil {
		return err
	}

	// graceful degradation here: only show error message if failed
	if err = u.Queue.ClickEvent(groupID, slotID, bannerID); err != nil {
		log.Println("rabbitmq ERROR:", err)
	}

	return nil
}

func FilterStats(bannerIDs []int64, stats []*entity.Stat) (result []*entity.Stat) {
	log.Printf("len(banners): %d, len(stats): %d\n", len(bannerIDs), len(stats))
	// sort stats
	sort.Slice(stats, func(i, j int) bool {
		return stats[i].BannerID < stats[j].BannerID
	})
	// sort ids
	sort.Slice(bannerIDs, func(i, j int) bool {
		return bannerIDs[i] < bannerIDs[j]
	})
	// find intersection
	i, j := 0, 0
	for i < len(bannerIDs) {
		switch {
		case j == len(stats):
			fallthrough
		case bannerIDs[i] < stats[j].BannerID:
			log.Println("no stats for banner", bannerIDs[i], ", initialize with zeros")

			result = append(result, &entity.Stat{
				BannerID: bannerIDs[i],
				Clicks:   0,
				Shows:    0,
			})
			i++
		case bannerIDs[i] > stats[j].BannerID:
			log.Println("skip stats for banner", stats[j].BannerID, ", it's removed")
			j++
		default:
			log.Println("found stats for banner", bannerIDs[i])

			result = append(result, stats[j])
			i++
			j++
		}
	}

	return result
}
