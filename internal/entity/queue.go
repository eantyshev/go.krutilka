package entity

import "time"

type EventType string

const (
	ClickEvent EventType = "click"
	ShowEvent  EventType = "show"
)

type BannerEvent struct {
	Timestamp time.Time `json:"timestamp"`
	Type      EventType `json:"type"`
	GroupID   int64     `json:"group_id"`
	SlotID    int64     `json:"slot_id"`
	BannerID  int64     `json:"banner_id"`
}
