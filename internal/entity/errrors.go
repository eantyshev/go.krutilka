package entity

import "fmt"

var (
	ErrBannerExists   = fmt.Errorf("banner already exists")
	ErrBannerNotFound = fmt.Errorf("banner not found")
	ErrNoSlotBanners  = fmt.Errorf("no banners for this slot")
)
