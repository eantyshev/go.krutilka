package entity

type Slot struct {
	ID int64
}

type Banner struct {
	ID          int64  `json:"banner_id"`
	Description string `json:"description"`
}

type Group struct {
	ID int64
}

type Stat struct {
	BannerID      int64
	Clicks, Shows int64
}
