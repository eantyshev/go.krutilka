package api

import (
	"encoding/json"
	"net/http"
	"os"
	"time"

	"gitlab.com/eantyshev/go.krutilka/internal/queue"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/eantyshev/go.krutilka/internal/entity"
	"gitlab.com/eantyshev/go.krutilka/internal/repository"
	"gitlab.com/eantyshev/go.krutilka/internal/usecase"
)

type Server struct {
	httpSrv *http.Server
	Usecase usecase.Iface
}

func NewServer(
	addrPort string,
	timeout time.Duration,
	repo repository.RepoIface,
	queueService queue.Service,
) *Server {
	srv := &Server{
		Usecase: &usecase.Usecase{
			Repo:  repo,
			Queue: queueService,
		},
	}
	r := mux.NewRouter()
	postRoute := r.Methods(http.MethodPost).Subrouter()
	postRoute.HandleFunc("/add_banner", srv.AddBannerHandler)
	postRoute.HandleFunc("/remove_banner", srv.RemoveBannerHandler)
	postRoute.HandleFunc("/click", srv.ClickHandler)
	postRoute.HandleFunc("/choose_banner", srv.ChooseBannerHandler)

	wrappedHandler := handlers.ContentTypeHandler(
		handlers.LoggingHandler(os.Stdout, r),
		"application/json",
	)

	srv.httpSrv = &http.Server{
		Addr:         addrPort,
		Handler:      wrappedHandler,
		ReadTimeout:  timeout,
		WriteTimeout: timeout,
	}

	return srv
}

func (s *Server) ServeForever() {
	panic(s.httpSrv.ListenAndServe())
}

func (s *Server) AddBannerHandler(w http.ResponseWriter, req *http.Request) {
	var addReq AddBannerRequest

	if err := json.NewDecoder(req.Body).Decode(&addReq); err != nil {
		WriteResponse(w, http.StatusBadRequest, err)
		return
	}

	if err := s.Usecase.AddBanner(addReq.SlotID, addReq.BannerID, addReq.Description); err != nil {
		statusCode := 500
		if err == entity.ErrBannerExists {
			statusCode = 409
		}

		WriteResponse(w, statusCode, err)

		return
	}

	WriteResponse(w, 200, nil)
}

func (s *Server) RemoveBannerHandler(w http.ResponseWriter, req *http.Request) {
	var removeReq RemoveBannerRequest

	if err := json.NewDecoder(req.Body).Decode(&removeReq); err != nil {
		WriteResponse(w, http.StatusBadRequest, err)
		return
	}

	if err := s.Usecase.RemoveBanner(removeReq.SlotID, removeReq.BannerID); err != nil {
		statusCode := 500
		if err == entity.ErrBannerNotFound {
			statusCode = 404
		}

		WriteResponse(w, statusCode, err)

		return
	}

	WriteResponse(w, 200, nil)
}

func (s *Server) ChooseBannerHandler(w http.ResponseWriter, req *http.Request) {
	var chooseReq ChooseBannerRequest

	if err := json.NewDecoder(req.Body).Decode(&chooseReq); err != nil {
		WriteResponse(w, http.StatusBadRequest, err)
		return
	}

	id, err := s.Usecase.ChooseBanner(chooseReq.GroupID, chooseReq.SlotID)
	if err != nil {
		statusCode := 500
		if err == entity.ErrNoSlotBanners {
			statusCode = 404
		}

		WriteResponse(w, statusCode, err)

		return
	}

	WriteResponse(w, 200, id)
}

func (s *Server) ClickHandler(w http.ResponseWriter, req *http.Request) {
	var clickReq ClickRequest

	if err := json.NewDecoder(req.Body).Decode(&clickReq); err != nil {
		WriteResponse(w, http.StatusBadRequest, err)
		return
	}

	err := s.Usecase.ClickBanner(clickReq.GroupID, clickReq.SlotID, clickReq.BannerID)
	if err != nil {
		statusCode := 500
		if err == entity.ErrBannerNotFound {
			statusCode = 404
		}

		WriteResponse(w, statusCode, err)

		return
	}

	WriteResponse(w, 200, nil)
}
