package api

type AddBannerRequest struct {
	SlotID      int64  `json:"slot_id"`
	BannerID    int64  `json:"banner_id"`
	Description string `json:"description"`
}

type RemoveBannerRequest struct {
	SlotID   int64 `json:"slot_id"`
	BannerID int64 `json:"banner_id"`
}

type ClickRequest struct {
	GroupID  int64 `json:"group_id"`
	SlotID   int64 `json:"slot_id"`
	BannerID int64 `json:"banner_id"`
}

type ChooseBannerRequest struct {
	SlotID  int64 `json:"slot_id"`
	GroupID int64 `json:"group_id"`
}

type BaseResponse struct {
	Error string `json:"error,omitempty"`
	ID    int64  `json:"id,omitempty"`
}
