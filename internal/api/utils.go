package api

import (
	"encoding/json"
	"net/http"
)

func WriteResponse(w http.ResponseWriter, statusCode int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	resp := BaseResponse{}
	switch res := data.(type) {
	case error:
		resp.Error = res.Error()
	case int64:
		resp.ID = res
	}

	if err := json.NewEncoder(w).Encode(resp); err != nil {
		panic(err)
	}
}
