package ucb1

import (
	"log"
	"math"

	"gitlab.com/eantyshev/go.krutilka/internal/entity"
)

func Choose(stats []*entity.Stat) (bannerID int64) {
	var (
		maxScore, curScore float64
		totalShows         int64
	)
	// count total shows, return if a new banner exists
	for _, stat := range stats {
		// If banner is not shown yet, give it a chance now
		if stat.Shows == 0 {
			log.Println("choosing banner", stat.BannerID, ", which is not yet shown")
			return stat.BannerID
		}

		totalShows += stat.Shows
	}

	maxScore = -1 // initialize to be less than minimal possible value 0
	lnTotal := math.Log(float64(totalShows))
	// calculate scores
	for _, stat := range stats {
		curScore = float64(stat.Clicks)/float64(stat.Shows) + math.Sqrt(2*lnTotal/float64(stat.Shows))
		log.Println("banner", stat.BannerID, "score = ", curScore)

		if maxScore < curScore {
			maxScore = curScore
			bannerID = stat.BannerID
		}
	}

	return bannerID
}
