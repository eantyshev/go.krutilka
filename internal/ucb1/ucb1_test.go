package ucb1

import (
	"log"
	"testing"

	"gitlab.com/eantyshev/go.krutilka/internal/entity"
)

//nolint:gomnd
func TestChoose(t *testing.T) {
	for i, testcase := range []struct {
		description string
		stats       []*entity.Stat
		expectedID  int64
	}{
		{
			"new banner appeared",
			[]*entity.Stat{
				{BannerID: 0, Clicks: 0, Shows: 1},
				{BannerID: 1, Clicks: 0, Shows: 1},
				{BannerID: 2, Clicks: 0, Shows: 1},
				{BannerID: 3, Clicks: 0, Shows: 0},
				{BannerID: 4, Clicks: 0, Shows: 1},
			},
			3,
		},
		{
			"most clickable banner",
			[]*entity.Stat{
				{BannerID: 0, Clicks: 1, Shows: 10},
				{BannerID: 1, Clicks: 2, Shows: 10},
				{BannerID: 2, Clicks: 3, Shows: 10},
				{BannerID: 3, Clicks: 2, Shows: 10},
			},
			2,
		},
		{
			"most rarely shown banner",
			[]*entity.Stat{
				{BannerID: 0, Clicks: 100, Shows: 100},
				{BannerID: 1, Clicks: 10, Shows: 100},
				{BannerID: 2, Clicks: 30, Shows: 100},
				{BannerID: 3, Clicks: 30, Shows: 100},
				{BannerID: 4, Clicks: 0, Shows: 1},
			},
			4,
		},
		{
			"banner with 0 clicks",
			[]*entity.Stat{
				{BannerID: 1, Clicks: 0, Shows: 1},
			},
			1,
		},
	} {
		log.Printf("case %d: %s\n", i, testcase.description)
		id := Choose(testcase.stats)

		if id != testcase.expectedID {
			t.Errorf("case %d expected %d, got %d", i, testcase.expectedID, id)
		}
	}
}
