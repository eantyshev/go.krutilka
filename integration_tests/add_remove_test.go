package main

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

//nolint:gomnd
func TestAddRemoveBanner(t *testing.T) {
	baseURL := os.Getenv("KRUTILKA_API_URL")
	client := NewKrutilkaClient(baseURL, 10*time.Second)

	t.Run("add_banner_new", func(t *testing.T) {
		code, _ := client.AddBanner(1, 2, "descr 2")
		assert.Equal(t, code, 200)
	})
	t.Run("banner_exists", func(t *testing.T) {
		code, resp := client.AddBanner(1, 2, "")
		assert.Equal(t, code, 409)
		assert.Equal(t, resp.Error, "banner already exists")
	})
	t.Run("add_banner_same_slot", func(t *testing.T) {
		code, _ := client.AddBanner(1, 4, "descr 4")
		assert.Equal(t, code, 200)
	})
	t.Run("existing_banners_another_slot", func(t *testing.T) {
		code, _ := client.AddBanner(3, 2, "")
		assert.Equal(t, code, 200)
		code, _ = client.AddBanner(3, 4, "")
		assert.Equal(t, code, 200)
	})
	t.Run("remove_banner", func(t *testing.T) {
		code, _ := client.RemoveBanner(1, 2)
		assert.Equal(t, code, 200)
	})
	t.Run("remove_banner_repeat", func(t *testing.T) {
		code, resp := client.RemoveBanner(1, 2)
		assert.Equal(t, code, 404)
		assert.Equal(t, resp.Error, "banner not found")
	})
	t.Run("banner_has_slots", func(t *testing.T) {
		code, resp := client.AddBanner(3, 2, "")
		assert.Equal(t, code, 409)
		assert.Equal(t, resp.Error, "banner already exists")
	})
	t.Run("remove_banner_all_slots", func(t *testing.T) {
		code, _ := client.RemoveBanner(3, 2)
		assert.Equal(t, code, 200)
		code, _ = client.RemoveBanner(1, 4)
		assert.Equal(t, code, 200)
		code, _ = client.RemoveBanner(3, 4)
		assert.Equal(t, code, 200)
	})
}
