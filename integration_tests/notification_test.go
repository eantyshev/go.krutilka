package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"github.com/streadway/amqp"
)

var amqpDSN = os.Getenv("KRUTILKA_RABBIT_DSN")

func init() {
	if amqpDSN == "" {
		amqpDSN = "amqp://guest:guest@localhost:5672/"
	}
}

const (
	amqpQueue    = "krutilka.all_events"
	amqpConsumer = ""
)

type BannerEvent struct {
	Timestamp time.Time `json:"timestamp"`
	Type      string    `json:"type"`
	GroupID   int64     `json:"group_id"`
	SlotID    int64     `json:"slot_id"`
	BannerID  int64     `json:"banner_id"`
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

type notifyTest struct {
	conn          *amqp.Connection
	ch            *amqp.Channel
	messages      []*BannerEvent
	messagesMutex *sync.RWMutex
	stopSignal    chan struct{}
}

func (test *notifyTest) startConsuming() {

	test.messagesMutex = new(sync.RWMutex)
	test.stopSignal = make(chan struct{})

	var err error

	test.conn, err = amqp.DialConfig(
		amqpDSN,
		amqp.Config{
			Heartbeat: 10 * time.Second,
			Locale:    "en_US",
			Dial:      amqp.DefaultDial(60 * time.Second),
		})
	panicOnErr(err)

	test.ch, err = test.conn.Channel()
	panicOnErr(err)

	events, err := test.ch.Consume(amqpQueue, amqpConsumer,
		true, false, false, false, nil)
	panicOnErr(err)

	go func(stop <-chan struct{}) {
		for {
			select {
			case <-stop:
				return
			case event := <-events:
				be := &BannerEvent{}
				err := json.Unmarshal(event.Body, be)
				panicOnErr(err)
				test.messagesMutex.Lock()
				test.messages = append(test.messages, be)
				test.messagesMutex.Unlock()
			}
		}
	}(test.stopSignal)
}

func (test *notifyTest) stopConsuming() {
	test.stopSignal <- struct{}{}

	panicOnErr(test.ch.Close())
	panicOnErr(test.conn.Close())
	test.messages = nil
}

func (test *notifyTest) notificationIsReceivedWithinSeconds(
	timeoutSec int,
	groupID, slotID, bannerID int64,
	eventType string,
) error {
	expected := BannerEvent{
		Type:     eventType,
		GroupID:  groupID,
		SlotID:   slotID,
		BannerID: bannerID,
	}
	time.Sleep(time.Duration(timeoutSec) * time.Second) // На всякий случай ждём обработки евента

	test.messagesMutex.RLock()
	defer test.messagesMutex.RUnlock()

	log.Printf("%d notifications received\n", len(test.messages))
	var event *BannerEvent
	for len(test.messages) >= 0 {
		event, test.messages = test.messages[0], test.messages[1:]
		expected.Timestamp = event.Timestamp
		if expected == *event {
			return nil
		}
	}
	return fmt.Errorf("notification wasn't received")
}

//nolint:gomnd
func TestNotifications(t *testing.T) {
	var (
		groupID  int64 = 55555
		slotID   int64 = 12345
		bannerID int64 = 54321
	)
	notifications := &notifyTest{}
	notifications.startConsuming()

	baseURL := os.Getenv("KRUTILKA_API_URL")
	client := NewKrutilkaClient(baseURL, 10*time.Second)

	code, _ := client.AddBanner(slotID, bannerID, "some descr")
	assert.Equal(t, code, 200)

	t.Run("show_event", func(t *testing.T) {
		code, _ := client.ChooseBanner(groupID, slotID)
		assert.Equal(t, code, 200)
		err := notifications.notificationIsReceivedWithinSeconds(10,
			groupID, slotID, bannerID, "show")
		if err != nil {
			t.Error(err)
		}
	})

	t.Run("click_event", func(t *testing.T) {
		code, _ := client.Click(groupID, slotID, bannerID)
		assert.Equal(t, code, 200)
		err := notifications.notificationIsReceivedWithinSeconds(10,
			groupID, slotID, bannerID, "click")
		if err != nil {
			t.Error(err)
		}
	})

	code, _ = client.RemoveBanner(slotID, bannerID)
	assert.Equal(t, code, 200)

	notifications.stopConsuming()
}
