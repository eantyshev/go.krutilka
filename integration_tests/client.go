package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

type AddBannerRequest struct {
	SlotID      int64  `json:"slot_id"`
	BannerID    int64  `json:"banner_id"`
	Description string `json:"description"`
}

type RemoveBannerRequest struct {
	SlotID   int64 `json:"slot_id"`
	BannerID int64 `json:"banner_id"`
}

type ClickRequest struct {
	GroupID  int64 `json:"group_id"`
	SlotID   int64 `json:"slot_id"`
	BannerID int64 `json:"banner_id"`
}

type ChooseBannerRequest struct {
	SlotID  int64 `json:"slot_id"`
	GroupID int64 `json:"group_id"`
}

type BaseResponse struct {
	Error string `json:"error,omitempty"`
	ID    int64  `json:"id,omitempty"`
}

type KrutilkaClient struct {
	*http.Client
	baseURL string
}

func NewKrutilkaClient(baseURL string, timeout time.Duration) *KrutilkaClient {
	c := &http.Client{
		Timeout: timeout,
	}

	client := &KrutilkaClient{
		Client:  c,
		baseURL: baseURL,
	}
	client.waitHttp()

	return client
}

func (c *KrutilkaClient) waitHttp() {
	deadline := time.Now().Add(c.Timeout)
	for deadline.After(time.Now()) {
		_, err := c.Get(c.baseURL)
		if err == nil {
			return
		}

		log.Println("waitHttp:", err)
		time.Sleep(time.Second)
	}
}

func (c *KrutilkaClient) requestDo(url string, req interface{}) (int, *BaseResponse) {
	data, err := json.Marshal(req)
	if err != nil {
		panic(err)
	}

	log.Println("url:", url)
	resp, err := c.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	fmt.Printf("%d ", resp.StatusCode)

	tee := io.TeeReader(resp.Body, os.Stdout)
	responseData := new(BaseResponse)
	err = json.NewDecoder(tee).Decode(responseData)

	if err != nil {
		panic(err)
	}

	return resp.StatusCode, responseData
}

func (c *KrutilkaClient) AddBanner(slotID, bannerID int64, description string) (int, *BaseResponse) {
	url := c.baseURL + "/add_banner"
	reqData := AddBannerRequest{
		SlotID:      slotID,
		BannerID:    bannerID,
		Description: description,
	}

	return c.requestDo(url, reqData)
}

func (c *KrutilkaClient) RemoveBanner(slotID, bannerID int64) (int, *BaseResponse) {
	url := c.baseURL + "/remove_banner"
	reqData := RemoveBannerRequest{
		SlotID:   slotID,
		BannerID: bannerID,
	}

	return c.requestDo(url, reqData)
}

func (c *KrutilkaClient) Click(groupID, slotID, bannerID int64) (int, *BaseResponse) {
	url := c.baseURL + "/click"
	reqData := ClickRequest{
		GroupID:  groupID,
		SlotID:   slotID,
		BannerID: bannerID,
	}

	return c.requestDo(url, reqData)
}

func (c *KrutilkaClient) ChooseBanner(groupID, slotID int64) (int, *BaseResponse) {
	url := c.baseURL + "/choose_banner"
	reqData := ChooseBannerRequest{
		GroupID: groupID,
		SlotID:  slotID,
	}

	return c.requestDo(url, reqData)
}
