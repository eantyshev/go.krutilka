package main

import (
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

//nolint:gomnd
func TestChooseBanner(t *testing.T) {
	baseURL := os.Getenv("KRUTILKA_API_URL")
	client := NewKrutilkaClient(baseURL, 10*time.Second)

	// 2 slots and 3 banners
	var slotID, bannerID int64
	for slotID = 1; slotID <= 2; slotID++ {
		for bannerID = 1; bannerID <= 3; bannerID++ {
			code, _ := client.AddBanner(slotID, bannerID, "some descr")
			assert.Equal(t, code, 200)
		}
	}

	t.Run("choose_no_clicks", func(t *testing.T) {
		cnt := make(map[int64]int)
		for i := 0; i < 99; i++ {
			slotID := int64(1 + rand.Intn(2))
			code, resp := client.ChooseBanner(100, slotID)
			assert.Equal(t, code, 200)
			cnt[resp.ID]++
		}
		assert.Equal(t, cnt, map[int64]int{1: 33, 2: 33, 3: 33})
	})

	t.Run("choose_slot_2_clicks", func(t *testing.T) {
		cnt := make(map[int64]int)
		for i := 0; i < 99; i++ {
			slotID := int64(1 + rand.Intn(2))
			code, _ := client.Click(100, slotID, 3)
			assert.Equal(t, code, 200)
			code, resp := client.ChooseBanner(100, 1)
			assert.Equal(t, code, 200)
			cnt[resp.ID]++
		}
		assert.Equal(t, cnt, map[int64]int{3: 99})
	})
	t.Run("choose_slot_2_noclick_again", func(t *testing.T) {
		// remove the most clicked banner
		code, _ := client.RemoveBanner(1, 3)
		assert.Equal(t, code, 200)
		code, _ = client.RemoveBanner(2, 3)
		assert.Equal(t, code, 200)

		cnt := make(map[int64]int)
		for i := 0; i < 100; i++ {
			code, resp := client.ChooseBanner(100, 2)
			assert.Equal(t, code, 200)
			cnt[resp.ID]++
		}
		assert.Equal(t, cnt, map[int64]int{1: 50, 2: 50})
	})

	// cleanup
	for slotID = 1; slotID <= 2; slotID++ {
		for bannerID = 1; bannerID <= 2; bannerID++ {
			code, _ := client.RemoveBanner(slotID, bannerID)
			assert.Equal(t, code, 200)
		}
	}
}
