package main

import (
	"flag"
	"os"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	flag.Parse()
	time.Sleep(10 * time.Second)
	os.Exit(m.Run())
}
