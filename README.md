[![Build Status](https://gitlab.com/eantyshev/go.krutilka/badges/master/pipeline.svg)](https://gitlab.com/eantyshev/go.krutilka/pipelines)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/eantyshev/go.krutilka)](https://goreportcard.com/report/gitlab.com/eantyshev/go.krutilka)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

# go.krutilka

Multi-armed bandit for banners rotation

## Entities and business logic
__Banners__ are the advertisement items.
__Slots__ are the places on a web page where certain banners can be shown.
Binding is required to show banner X in the slot Y. Banner can only be shown in the slots which it was bound to.
Bindings are  created by `POST /add_banner` call, removed by `POST /remove_banner` call.
Every client belongs to a __group__ which has similar preferences and likelihood to click on the same banner.
To decide which banner is to be shown for a group Z and slot Y `POST /choose_banner` is used.

To collect the feedback, there are __show__ and __click__ event types.
These are consumed via RabbitMQ, but for illustrative purposes
there are endpoints `POST /show` and `POST /click` too.

UCB1 is an euristic algorithm which governs how banners are shown.
* More popular banners are shown more often to maximize the "reward"
* Rarely shown (and new) banners always have a chance to be shown

_id_ -- 64bit integers

## Manual use

Execute `make run` to make the REST API listening at localhost:8888
You can also connect to Redis and monitor the serialized content at localhost:7379
* Create banner 1 and bind it to the slot 4:
> curl -v -H "Content-Type: application/json" http://localhost:8888/add_banner -d '{"slot_id": 4, "banner_id": 1, "description": "descr 22"}'
* Bind banner 1 to the slot 5
> curl -v -H "Content-Type: application/json" http://localhost:8888/add_banner -d '{"slot_id": 5, "banner_id": 1}'
* Unbound banner 1 from the slot 4
> curl -v -H "Content-Type: application/json" http://localhost:8888/remove_banner -d '{"slot_id": 4, "banner_id": 1}'
* Choose a banner to show for group 100 in the slot 5
> curl -v -H "Content-Type: application/json" http://localhost:8888/choose_banner -d '{"slot_id": 5, "group_id": 100}'
> 
> {"id": 1}
* Add clicks to the banner 1 in the slot 5 and group 100
> curl -v -H "Content-Type: application/json" http://localhost:8888/click -d '{"slot_id": 5, "banner_id": 1, "group_id": 100}

## Unit tests

```make unittest```

UCB1 algorithm is covered by unit tests.

## Integration tests

`make test` makes a testing environment and runs integration tests from a separate container.
