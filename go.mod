module gitlab.com/eantyshev/go.krutilka

go 1.13

require (
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.6.2
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	github.com/stretchr/testify v1.4.0
)
