# Environment
FROM golang:1.13.3 as build-env

RUN mkdir -p /opt/krutilka
WORKDIR /opt/krutilka
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 go build -o /opt/service/krutilka_api

# Release
FROM alpine:latest
COPY --from=build-env /opt/service/krutilka_api /bin/krutilka_api
RUN mkdir /etc/krutilka
COPY example_config.yaml /etc/krutilka/config.yaml
ENTRYPOINT ["/bin/krutilka_api", "--config", "/etc/krutilka/config.yaml"]
