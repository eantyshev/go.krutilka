package main

import (
	"strings"

	"gitlab.com/eantyshev/go.krutilka/internal/queue"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/eantyshev/go.krutilka/internal/api"
	"gitlab.com/eantyshev/go.krutilka/internal/repository/redis"
)

func LoadConfig(cfgPath string) {
	viper.SetEnvPrefix("krutilka")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
	viper.SetConfigFile(cfgPath)

	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}
}

func main() {
	configPath := pflag.StringP("config", "c", "config.yaml", "configuration file")

	pflag.Parse()
	LoadConfig(*configPath)

	addrPort := viper.GetString("http.listen")
	timeout := viper.GetDuration("http.timeout")

	redisAddr := viper.GetString("redis.addr")
	redisPoolSize := viper.GetInt("redis.poolsize")
	redisIdleTimeout := viper.GetDuration("redis.idle_timeout")

	amqpDsn := viper.GetString("rabbit.dsn")
	exchangeName := viper.GetString("rabbit.exchange")
	queueName := viper.GetString("rabbit.queue")
	amqpConnectTimeout := viper.GetDuration("rabbit.connect_timeout")
	amqpRetryPeriod := viper.GetDuration("rabbit.retry_period")

	repo := redis.NewRedisRepo(redisAddr, redisPoolSize, redisIdleTimeout)
	queueSvc := queue.NewRabbitQueue(
		amqpDsn, exchangeName, queueName,
		amqpConnectTimeout, amqpRetryPeriod,
	)
	srv := api.NewServer(addrPort, timeout, repo, queueSvc)
	srv.ServeForever()
}
